function [s11,s12,s13,s22,s23,s33]=computeStressVerticalShearZoneFiniteDifference( ...
    x1,x2,x3,q1,q2,q3,L,T,W,theta,epsv11p,epsv12p,epsv13p,epsv22p,epsv23p,epsv33p,G,nu)
% function COMPUTESTRESSVERTICALSHEARZONEFINITEDIFFERENCE computes the
% stress field associated with deforming vertical shear zones using the 
% finite difference approximation solution considering the geometry:
%
%                      N (x1)
%                     /
%                    /| strike (theta)          E (x2)
%        q1,q2,q3 ->@--------------------------+
%                   |                        w |     +
%                   |                        i |    /
%                   |                        d |   / s
%                   |                        t |  / s
%                   |                        h | / e
%                   |                          |/ n
%                   +--------------------------+  k
%                   :       l e n g t h       /  c
%                   |                        /  i
%                   :                       /  h
%                   |                      /  t
%                   :                     /
%                   |                    +
%                   Z (x3)
%
%
% Input:
% x1, x2, x3         northing, easting, and depth of the observation point,
% q1, q2, q3         north, east and depth coordinates of the shear zone,
% L, T, W            length, thickness, and width of the shear zone,
% theta (degree)     strike of the shear zone,
% epsvijp            source strain component 11, 12, 13, 22, 23 and 33 
%                    in the shear zone in the system of reference tied to 
%                    the shear zone,
% G, nu              shear modulus and Poisson's ratio in the half space.
%
% Output:
% sij                 stress component in the ij direction.
%
%-----------------------------------------------------------------------
%  Author: Sylvain Barbot (sbarbot@ntu.edu.sg) - 
%  Earth Observatory of Singapore
%  Copyright (c) 2017 Sylvain Barbot
%
%  This code and related code should be cited as:
%    Barbot S., J. D. P. Moore and V. Lambert, Displacement and Stress
%    Associated with Distributed Anelastic Deformation in a Half Space,
%    Bull. Seism. Soc. Am., 107(2), 10.1785/0120160237, 2017.
% 
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, including
% without limitation the rights to use, copy, modify, merge, publish,
% distribute, sublicense, and/or sell copies of the Software, and to permit
% persons to whom the Software is furnished to do so, subject to the
% following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
% NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
% OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
% USE OR OTHER DEALINGS IN THE SOFTWARE.
%
%-----------------------------------------------------------------------

% Lame parameter
lambda=G*2*nu/(1-2*nu);

dx=1e-4;

[u1d1p,u2d1p,u3d1p]=computeDisplacementVerticalShearZone( ...
    x1+dx,x2,x3,q1,q2,q3,L,T,W,theta,epsv11p,epsv12p,epsv13p,epsv22p,epsv23p,epsv33p,G,nu);

[u1d1m,u2d1m,u3d1m]=computeDisplacementVerticalShearZone( ...
    x1-dx,x2,x3,q1,q2,q3,L,T,W,theta,epsv11p,epsv12p,epsv13p,epsv22p,epsv23p,epsv33p,G,nu);

[u1d2p,u2d2p,u3d2p]=computeDisplacementVerticalShearZone( ...
    x1,x2+dx,x3,q1,q2,q3,L,T,W,theta,epsv11p,epsv12p,epsv13p,epsv22p,epsv23p,epsv33p,G,nu);

[u1d2m,u2d2m,u3d2m]=computeDisplacementVerticalShearZone( ...
    x1,x2-dx,x3,q1,q2,q3,L,T,W,theta,epsv11p,epsv12p,epsv13p,epsv22p,epsv23p,epsv33p,G,nu);

[u1d3p,u2d3p,u3d3p]=computeDisplacementVerticalShearZone( ...
    x1,x2,x3+dx,q1,q2,q3,L,T,W,theta,epsv11p,epsv12p,epsv13p,epsv22p,epsv23p,epsv33p,G,nu);

[u1d3m,u2d3m,u3d3m]=computeDisplacementVerticalShearZone( ...
    x1,x2,x3-dx,q1,q2,q3,L,T,W,theta,epsv11p,epsv12p,epsv13p,epsv22p,epsv23p,epsv33p,G,nu);

% displacement gradients
e11=(u1d1p-u1d1m)/(2*dx);
e12=(u1d2p-u1d2m+u2d1p-u2d1m)/(4*dx);
e13=(u1d3p-u1d3m+u3d1p-u3d1m)/(4*dx);
e22=(u2d2p-u2d2m)/(2*dx);
e23=(u2d3p-u2d3m+u3d2p-u3d2m)/(4*dx);
e33=(u3d3p-u3d3m)/(2*dx);

% remove inelastic eigenstrain
Omega=@(x) heaviside(x+0.5)-heaviside(x-0.5);
S=@(x) Omega(x-0.5);

x1p= (x1-q1)*cosd(theta)+(x2-q2)*sind(theta);
x2p=-(x1-q1)*sind(theta)+(x2-q2)*cosd(theta);

epsv11=(cosd(theta)*epsv11p-sind(theta)*epsv12p)*cosd(theta)-(cosd(theta)*epsv12p-sind(theta)*epsv22p)*sind(theta);
epsv12=(cosd(theta)*epsv11p-sind(theta)*epsv12p)*sind(theta)+(cosd(theta)*epsv12p-sind(theta)*epsv22p)*cosd(theta);
epsv13=cosd(theta)*epsv13p-sind(theta)*epsv23p;
epsv22=(sind(theta)*epsv11p+cosd(theta)*epsv12p)*sind(theta)+(sind(theta)*epsv12p+cosd(theta)*epsv22p)*cosd(theta);
epsv23=sind(theta)*epsv13p+cosd(theta)*epsv23p;
epsv33=epsv33p;

e11=e11-epsv11*S(x1p/L).*Omega(x2p/T).*S((x3-q3)/W);
e12=e12-epsv12*S(x1p/L).*Omega(x2p/T).*S((x3-q3)/W);
e13=e13-epsv13*S(x1p/L).*Omega(x2p/T).*S((x3-q3)/W);
e22=e22-epsv22*S(x1p/L).*Omega(x2p/T).*S((x3-q3)/W);
e23=e23-epsv23*S(x1p/L).*Omega(x2p/T).*S((x3-q3)/W);
e33=e33-epsv33*S(x1p/L).*Omega(x2p/T).*S((x3-q3)/W);

% stress components
s11=lambda*(e11+e22+e33)+2*G*e11;
s12=2*G*e12;
s13=2*G*e13;
s22=lambda*(e11+e22+e33)+2*G*e22;
s23=2*G*e23;
s33=lambda*(e11+e22+e33)+2*G*e33;

    function y=heaviside(x)
        y=x>0;
    end

end








