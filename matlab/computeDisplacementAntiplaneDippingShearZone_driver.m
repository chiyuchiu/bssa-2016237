
% dimension
W=20e3; % width down dip
T=20e3; % thickness

% prescribe strain in the shear-zone-centric coordinate system
epsv12p=1e-6;
epsv13p=0e-6;

% position
q2=0e2;
q3=20e3; % depth

% orientation
phi=45;  % dip angle

% observation points
N=128;
dx2=1e3;
dx3=1e3;
x2=repmat((-N/2:(N/2-1))'*dx2,1,N);
x3=repmat((0:(N-1))*dx2,N,1);

tic
u1a=computeDisplacementAntiplaneDippingShearZone( ...
    x2,x3,q2,q3,T,W,phi,epsv12p,epsv13p);
u1n=computeDisplacementAntiplaneDippingShearZoneTanhSinh( ...
    x2,x3,q2,q3,T,W,phi,epsv12p,epsv13p);
toc

%% plot displacements

figure(62);clf;
cla;hold on;
axis equal tight
pcolor(x2/1e3,x3/1e3,u1a),shading flat;
%contour(x2/1e3,x3/1e3,u1,'color','k')
h=colorbar();
ylabel(h,'u_1 (m)');
set(gca,'YDir','reverse');
%set(gca,'xlim',[-1 1]*60,'ylim',[0 1]*120);
box on, grid on
xlabel('x_2 (km)');
ylabel('x_3 (km)');
