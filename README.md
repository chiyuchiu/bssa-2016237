# README #

We present a suite of analytical and semianalytical solutions for the displacements, strains, and stress
due to distributed anelastic deformation of finite strain volumes in a half‐space for cuboid sources. 
We provide the solutions in the cases of antiplane strain, plane strain, and 3D deformation. These 
expressions represent powerful tools for the analysis of deformation data to image distributed strain 
in the Earth’s interior and for the dynamic modeling of distributed deformation off faults, including 
thermoelasticity, poroelasticity, and viscoelasticity. We include computer programs that evaluate these 
expressions free of major singular points. Combined with formulas that describe the deformation around 
faults, these solutions represent a comprehensive description of quasi‐static deformation throughout 
the earthquake cycle within the lithosphere–asthenosphere system.

### Publications

Moore, James DP, Hang Yu, Chi-Hsien Tang, Teng Wang, Sylvain Barbot, Dongju Peng, Sagar Masuti, Justin Dauwels, 
Ya-Ju Hsu, Valère Lambert, Priyamvada Nanjundiah, Shengji Wei, Eric Lindsey, Lujia Feng and Bunichiro Shibazaki.
"Imaging the distribution of transient viscosity following the 2016 Mw 7.1 Kumamoto earthquake", Science (2017).

Barbot, Sylvain, James DP Moore, and Valère Lambert. "Displacement and Stress Associated with Distributed
Anelastic Deformation in a Half‐Space." Bulletin of the Seismological Society of America 107.2 (2017): 821-855.

Lambert, Valère, and Sylvain Barbot. "Contribution of viscoelastic flow in earthquake cycles within the 
lithosphere‐asthenosphere system." Geophysical Research Letters 43.19 (2016).

### What is this repository for? ###

This repository is to provide scientific computing libraries to evaluate the displacement and stress
caused by distributed anelastic deformation in a half space. The kernels allow linear inversion of
deformation data for distributed deformation in the crust or mantle, or dynamic simulations of 
viscoelastic and poroelastic behavior using the integral method.