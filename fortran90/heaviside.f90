
!------------------------------------------------------------------------
!> function heaviside
!! computes the Heaviside function
!------------------------------------------------------------------------
REAL*8 FUNCTION heaviside(x)
  IMPLICIT NONE
  REAL*8, INTENT(IN) :: x

   IF (0 .LT. x) THEN
      heaviside=1.0_8
   ELSE
      heaviside=0.0_8
   END IF

END FUNCTION heaviside

